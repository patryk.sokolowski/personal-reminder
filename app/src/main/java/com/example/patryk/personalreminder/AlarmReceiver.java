package com.example.patryk.personalreminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import static android.app.NotificationChannel.DEFAULT_CHANNEL_ID;

/**
 * Created by Patryk Sokołowski on 19.03.18.
 */

public class AlarmReceiver extends BroadcastReceiver {

    public int getNotificationID() {
        return (int) System.currentTimeMillis();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String intentExtras = intent.getStringExtra(Intent.EXTRA_TEXT);
        Intent newIntent = new Intent(context, AlarmViewMain.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        newIntent.putExtra(Intent.EXTRA_TEXT, intentExtras);
        context.startActivity(newIntent);

        String[] splitContent = intentExtras.split(context.getString(R.string.regex_separator));

        String notificationContentTitle = context.getString(R.string.notification_content_title);
        String notificationContentText = context.getString(R.string.notification_title);
        String notificationDueDate = context.getString(R.string.notification_duedate);
        String notificationDueTime = context.getString(R.string.notification_duetime);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context.getApplicationContext(), DEFAULT_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notifications)
                .setContentTitle(notificationContentTitle)
                .setContentText(notificationContentText + ": " + splitContent[1])
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(notificationContentText + ": " + splitContent[1] + "\n" + notificationDueDate + ": " +
                                splitContent[3] + "\n" + notificationDueTime + ": " + splitContent[4]))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context.getApplicationContext());
        notificationManager.notify(getNotificationID(), mBuilder.build());
    }
}
