package com.example.patryk.personalreminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class ReminderMain extends AppCompatActivity {
    final static int DEFAULT_INIT_VALUE_INT = -1;

    MyAdapter mAdapter;
    Calendar calendar = Calendar.getInstance();
    Calendar tempCalendar = Calendar.getInstance(TimeZone.getDefault());
    Calendar deleteReminderCalendar = Calendar.getInstance(TimeZone.getDefault());

    private ArrayList<Reminder> mStringList;
    private ArrayList<Reminder> listToSort;
    private RecyclerView mRecyclerView;
    private ArrayList<Integer> disabledIDs = new ArrayList<>();

    public void ShowToastInIntentService(final String sText) {
        final Context MyContext = ReminderMain.this;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MyContext, sText, Toast.LENGTH_LONG).show();
            }
        });
    }

    private Integer getPxForDp(Integer paddingInDP) {
        int padding_in_dp = paddingInDP;
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (padding_in_dp * scale + 0.5f);
    }

    private void setItemAccessibility(MyAdapter.ViewHolder view, Boolean isEnabled) {
        view.layoutContainer.setEnabled(isEnabled);
        view.layoutContainer.setFocusable(isEnabled);
        view.layoutContainer.setClickable(isEnabled);
        view.layoutContainer.setLongClickable(isEnabled);
    }

    private void setItemSizeAndStyle(MyAdapter.ViewHolder view, Boolean isReminder) {
        if (isReminder) {
            view.mTextView.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorSecondaryText, null));
            view.mTextView.setPadding(getPxForDp(2), getPxForDp(3), getPxForDp(2), 0);
            view.dTextView.setVisibility(View.VISIBLE);
            view.reminderDateWrapper.setVisibility(View.VISIBLE);
            view.layoutContainer.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorTransparent, null));
        } else {
            view.mTextView.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorWhite, null));
            view.mTextView.setPadding(getPxForDp(2), getPxForDp(6), getPxForDp(2), getPxForDp(6));
            view.dTextView.setVisibility(View.GONE);
            view.reminderDateWrapper.setVisibility(View.GONE);
            view.layoutContainer.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorBlackLightTinted, null));
        }
    }

    public Boolean intToBoolean(int value) {
        return value > 0;
    }

    String getMonthForInt(int num) {
        String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        String month = "";
//        DateFormatSymbols dfs = new DateFormatSymbols();
//        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11 ) {
//            month = months[num];
            month = months[num];
        }
        return month;
    }

    private ArrayList<Reminder> groupRemindersByMonths(ArrayList<Reminder> remindersList) {
        ArrayList<Integer> occurredMonths = new ArrayList<>();
        ArrayList<Reminder> finalList = new ArrayList<>();
        tempCalendar.setTimeInMillis(System.currentTimeMillis());

        for (int i = 0; i < remindersList.size(); i++) {
            String currentRemindersDate = remindersList.get(i).dateTime;
            if (!currentRemindersDate.equals(String.valueOf(DEFAULT_INIT_VALUE_INT)))
                tempCalendar.setTimeInMillis(Long.valueOf(currentRemindersDate));
            else
                tempCalendar.setTimeInMillis(System.currentTimeMillis());

            if (occurredMonths.isEmpty()) {
                occurredMonths.add(tempCalendar.get(Calendar.MONTH));
            } else {
                for (int j = 0; j < occurredMonths.size(); j++) {
                    if (tempCalendar.get(Calendar.MONTH) == Integer.valueOf(occurredMonths.get(j))) {
                        break;
                    } else if (tempCalendar.get(Calendar.MONTH) != Integer.valueOf(occurredMonths.get(j)) && (j + 1) >= occurredMonths.size()) {
                        occurredMonths.add(tempCalendar.get(Calendar.MONTH));
                        break;
                    }
                }
            }
        }

        for (int j = 0; j < occurredMonths.size(); j++) {
            String id = String.valueOf(Integer.MAX_VALUE - j);
            String title = getMonthForInt(occurredMonths.get(j)).toUpperCase();
            String description = null;
            String isImportant = String.valueOf(0);
            String dateTime = String.valueOf(System.currentTimeMillis());
            Reminder reminder = new Reminder(id, title, description, isImportant, dateTime);
            finalList.add(reminder);
            for (int k = 0; k < remindersList.size(); k++) {
                tempCalendar.setTimeInMillis(Long.valueOf(remindersList.get(k).dateTime));
                if (tempCalendar.get(Calendar.MONTH) == occurredMonths.get(j)) {
                    finalList.add(remindersList.get(k));
                }
            }
            disabledIDs.add(Integer.valueOf(id));
        }

        return finalList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent intent = new Intent(ReminderMain.this, ReminderCreatorMain.class);
                startActivity(intent);
            }
        });

        initList();

        Log.d("Activity", "onCreate");
    }

    private void initList() {
        RecyclerView.LayoutManager mLayoutManager;

        mRecyclerView = findViewById(R.id.reminderCollectionContainer);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter
        mStringList = new ArrayList<>();
        listToSort = readFromDatabase();
        ArrayList<Reminder> sortedList = groupRemindersByMonths(listToSort);
        mStringList = sortedList;

        TextView noRemindersText = findViewById(R.id.noRemindersText);

        if (mStringList.isEmpty()) {
            noRemindersText.setVisibility(View.VISIBLE);
        } else {
            noRemindersText.setVisibility(View.GONE);
        }

        mAdapter = new MyAdapter(mStringList);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void markImportantReminders(String isImportant, View statusBar) {
        if (Integer.valueOf(isImportant) != 0)
            statusBar.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorStrongRedAccent, null));
        else
            statusBar.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorTransparent, null));
    }

    private void markRemindersDueDate(String id, TextView dayField, TextView monthYearField) {
        final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(this);
        ArrayList<ArrayList<String>> allRemindersData = mydb.getCompleteRemindersData();

        int currentReminderID = Integer.valueOf(id);

        if (!disabledIDs.contains(currentReminderID)) {
            int itemIndex = 0;
            for (int i = 0; i < mydb.numberOfRows(); i++) {
                String currentRecordID = allRemindersData.get(i).get(0);
                if (currentRecordID.equals(id)) {
                    itemIndex = i;
                    break;
                }
            }
            Boolean isReminder = intToBoolean(Integer.valueOf(allRemindersData.get(itemIndex).get(9)));

            if (isReminder) {
                long currentReminderDueDate = Long.valueOf(allRemindersData.get(itemIndex).get(5));

                if (currentReminderDueDate != DEFAULT_INIT_VALUE_INT) {
                    calendar.setTimeInMillis(currentReminderDueDate);

                    int dayHelper = calendar.get(Calendar.DAY_OF_MONTH);
                    int monthHelper = calendar.get(Calendar.MONTH);
                    int yearHelper = calendar.get(Calendar.YEAR);
                    String initThisDay, initThisMonth;
                    if (dayHelper < 10)
                        initThisDay = "0" + dayHelper;
                    else
                        initThisDay = String.valueOf(dayHelper);
                    if ((monthHelper + 1) < 10)
                        initThisMonth = "0" + (monthHelper + 1);
                    else
                        initThisMonth = String.valueOf(monthHelper + 1);

                    dayField.setText(initThisDay);
                    String monthYearCombined = initThisMonth + "/" + yearHelper;
                    monthYearField.setText(monthYearCombined);
                } else {
                    dayField.setText(null);
                    monthYearField.setText(null);
                }
            }
        } else {
            dayField.setText(null);
            monthYearField.setText(null);
        }
        mydb.close();
    }

    @Override
    protected void onStart() {
        super.onStart();

        initList();

        Log.d("Activity", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();

//        getWindow().getDecorView().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mStringList.add("Nowy item");
//                mRecyclerView.getAdapter().notifyDataSetChanged();
//            }
//        }, 5000);

        Log.d("ActivityMain", "onResume");
    }

    class Reminder {
        String id;
        String title;
        String description;
        String isImportant;
        String dateTime;

        public Reminder(String id, String title, String description, String isImportant, String dateTime) {
            this.id = id;
            this.title = title;
            this.description = description;
            this.isImportant = isImportant;
            this.dateTime = dateTime;
        }
    }

    private ArrayList<Reminder> readFromDatabase() {
        ArrayList<Reminder> myList = new ArrayList<>();

        final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(this);

        ArrayList<ArrayList<String>> allRemindersData = mydb.getCompleteRemindersData();

        for (int i = 0; i < mydb.numberOfRows(); i++) {
            String id = allRemindersData.get(i).get(0);
            String title = allRemindersData.get(i).get(1);
            String description = allRemindersData.get(i).get(2);
            String isImportant = allRemindersData.get(i).get(3);
            String dateTime = allRemindersData.get(i).get(5);
            Reminder reminder = new Reminder(id, title, description, isImportant, dateTime);
            myList.add(reminder);
        }

        mydb.close();

        return myList;
    }

    private void deleteFromDatabase(Integer reminderID) {
        final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(this);

        int result = mydb.deleteReminder(reminderID);
        if (result != 0) {
            listToSort = readFromDatabase();
            mStringList = groupRemindersByMonths(listToSort);
            mAdapter.setmDataset(mStringList);
            ShowToastInIntentService(getString(R.string.reminder_delete_handler_text_success));
        } else {
            ShowToastInIntentService(getString(R.string.reminder_delete_handler_text_failure));
        }

        mydb.close();
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

        public List<Reminder> mDataset;

        public void setmDataset(List<Reminder> mDataset) {
            this.mDataset = mDataset;
            notifyDataSetChanged();

            TextView noRemindersText = findViewById(R.id.noRemindersText);
            if (mStringList.isEmpty()) {
                noRemindersText.setVisibility(View.VISIBLE);
            } else {
                noRemindersText.setVisibility(View.GONE);
            }

            TextView description = findViewById(R.id.description);
            if (description.getText().equals(null))
                description.setBackgroundColor(Color.parseColor(String.valueOf(ResourcesCompat.getColor(getResources(), R.color.colorStrongRedAccent, null))));
        }

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public TextView idTextView;
            public TextView mTextView;
            public TextView dTextView;
            public View iView;
            public TextView dayView;
            public TextView monthYearView;
            public RelativeLayout layoutContainer;
            public LinearLayout reminderDateWrapper;

            public ViewHolder(View v) {
                super(v);
                idTextView = v.findViewById(R.id.reminderID);
                mTextView = v.findViewById(R.id.title);
                dTextView = v.findViewById(R.id.description);
                iView = v.findViewById(R.id.reminderStatusBar);
                dayView = v.findViewById(R.id.reminderDateDayOfMonth);
                monthYearView = v.findViewById(R.id.reminderDateMonthAndYear);
                layoutContainer = v.findViewById(R.id.reminderItemSingle);
                reminderDateWrapper = v.findViewById(R.id.reminderDateWrapper);

                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String thisReminderID = idTextView.getText().toString();

                        Intent intent = new Intent(ReminderMain.this, ReminderItemViewMain.class);
                        intent.putExtra(Intent.EXTRA_TEXT, thisReminderID);
                        startActivity(intent);
                    }
                });

                v.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ReminderMain.this);

                        builder.setTitle(R.string.dialog_title_warning);
                        builder.setMessage(R.string.dialog_message_reminder_delete_action);
                        builder.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Integer thisReminderID = Integer.valueOf(idTextView.getText().toString());
                                final AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                Intent myIntent = new Intent(getApplicationContext(), AlarmReceiver.class);

                                final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(getApplicationContext());

                                ArrayList<String> thisReminderData = mydb.getReminderData(thisReminderID);
                                deleteReminderCalendar.setTimeInMillis(Long.valueOf(thisReminderData.get(5)));
                                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                                String fullDate = sdf.format(deleteReminderCalendar.getTimeInMillis());
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                                String fullTime = simpleDateFormat.format(deleteReminderCalendar.getTime());
                                String alarmExtras = fullDate + getString(R.string.regex_separator) + fullTime;

                                String intentExtras = idTextView.getText().toString() + getString(R.string.regex_separator) + mTextView.getText().toString() +
                                        getString(R.string.regex_separator) + dTextView.getText().toString() + getString(R.string.regex_separator) + alarmExtras;

                                mydb.close();

                                myIntent.putExtra(Intent.EXTRA_TEXT, intentExtras);
                                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), thisReminderID, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                                if (manager != null) {
                                    manager.cancel(pendingIntent);
                                }
                                deleteFromDatabase(thisReminderID);
                            }
                        });
                        builder.setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });

                        builder.create();
                        builder.show();
                        return false;
                    }
                });
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public MyAdapter(List<Reminder> data) {
            mDataset = data;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View v = inflater.inflate(R.layout.list_item, parent, false);

            return new ViewHolder(v);
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            Reminder reminder = mDataset.get(position);
            holder.idTextView.setText(reminder.id);
            holder.mTextView.setText(reminder.title);
            holder.dTextView.setText(reminder.description);

            Integer currentID = null;
            if (!holder.idTextView.getText().toString().trim().equals(""))
                currentID = Integer.valueOf(holder.idTextView.getText().toString());

            if (currentID != null && disabledIDs.contains(currentID)) {
                setItemAccessibility(holder, false);
                setItemSizeAndStyle(holder, false);
            } else {
                setItemAccessibility(holder, true);
                setItemSizeAndStyle(holder, true);
            }

            markImportantReminders(reminder.isImportant, holder.iView);
            markRemindersDueDate(reminder.id, holder.dayView, holder.monthYearView);
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mDataset.size();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reminder_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), SettingsMain.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
