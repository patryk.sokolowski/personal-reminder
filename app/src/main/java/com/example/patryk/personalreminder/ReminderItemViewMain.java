package com.example.patryk.personalreminder;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static android.app.NotificationChannel.DEFAULT_CHANNEL_ID;

/**
 * Created by Patryk Sokołowski on 13.03.18.
 */

public class ReminderItemViewMain extends AppCompatActivity implements Reminder {
    final static int DEFAULT_INIT_VALUE_INT = -1;
    private PopupWindow mPopupWindow;
    private Calendar calendar = Calendar.getInstance();

    final static int[] INPUT_LIMIT_MONTHS = {1, 11};
    final static int[] INPUT_LIMIT_WEEKS = {1, 3};
    final static int[] INPUT_LIMIT_DAYS = {1, 6};
    final static int[] INPUT_LIMIT_HOURS = {1, 23};
    final static int[] INPUT_LIMIT_MINUTES = {1, 59};

    public void ShowToastInIntentService(final String sText) {
        final Context MyContext = ReminderItemViewMain.this;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MyContext, sText, Toast.LENGTH_LONG).show();
            }
        });
    }

    public Boolean intToBoolean(int value) {
        return value > 0;
    }

    public void setSpinnerInputFocusability(EditText inputField, Boolean isEnabled) {
        inputField.setFocusableInTouchMode(isEnabled);
        inputField.setFocusable(isEnabled);
        inputField.setEnabled(isEnabled);
    }

    private void initReminder() {
        TextView thisReminderID = findViewById(R.id.singleReminderID);
        TextView thisReminderTitle = findViewById(R.id.singleReminderTitle);
        TextView thisReminderDescription = findViewById(R.id.singleReminderDescription);
        CheckBox isReminderImportant = findViewById(R.id.checkboxIsReminderImportant);
        Switch isNoteOrReminder = findViewById(R.id.isNoteOrReminder);
        Switch isAlarmSet = findViewById(R.id.toggleAlarm);
        final Spinner spinner = findViewById(R.id.timePeriodSpinner);
        EditText timePeriodInput = findViewById(R.id.remindBeforeDueDateValue);
        LinearLayout reminderSettingsContainer = findViewById(R.id.reminderSettingsContainer);

        int passedReminderID = Integer.valueOf(getIntent().getStringExtra(Intent.EXTRA_TEXT));

        ArrayList<String> fetchedData;

        final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(this);

        fetchedData = mydb.getReminderData(passedReminderID);

        mydb.close();

        thisReminderID.setText(String.valueOf(passedReminderID));
        thisReminderTitle.setText(fetchedData.get(1));
        thisReminderDescription.setText(fetchedData.get(2));
        isReminderImportant.setChecked(intToBoolean(Integer.valueOf(fetchedData.get(3))));
        isAlarmSet.setChecked(intToBoolean(Integer.valueOf(fetchedData.get(4))));
        long reminderDate = Long.valueOf(fetchedData.get(5));
        if (reminderDate != DEFAULT_INIT_VALUE_INT)
            calendar.setTimeInMillis(reminderDate);
        else
            calendar.setTimeInMillis(calendar.getTimeInMillis());
        final Integer chosenSpinnerID = Integer.valueOf(fetchedData.get(7));
        timePeriodInput.setText(fetchedData.get(8));
        isNoteOrReminder.setChecked(intToBoolean(Integer.valueOf(fetchedData.get(9))));

        if (isNoteOrReminder.isChecked())
            reminderSettingsContainer.setVisibility(View.VISIBLE);
        else
            reminderSettingsContainer.setVisibility(View.INVISIBLE);

        if (chosenSpinnerID != DEFAULT_INIT_VALUE_INT) {
            spinner.post(new Runnable() {
                @Override
                public void run() {
                    spinner.setSelection(chosenSpinnerID);
                }
            });
        }
    }

    private void saveCurrentRemindersData() {
        TextView idField = findViewById(R.id.singleReminderID);
        TextView titleField = findViewById(R.id.singleReminderTitle);
        TextView descriptionField = findViewById(R.id.singleReminderDescription);
        Switch isNoteOrReminderSwitch = findViewById(R.id.isNoteOrReminder);
        CheckBox isImportantField = findViewById(R.id.checkboxIsReminderImportant);
        Switch isAlarmSetSwitch = findViewById(R.id.toggleAlarm);
        Spinner spinner = findViewById(R.id.timePeriodSpinner);
        EditText remindValueField = findViewById(R.id.remindBeforeDueDateValue);
        String title = titleField.getText().toString();
        String description = descriptionField.getText().toString();
        Boolean isReminder = isNoteOrReminderSwitch.isChecked();
        Boolean isImportant = isImportantField.isChecked();

        if (!isReminder)
            isAlarmSetSwitch.setChecked(isReminder);

        Boolean isAlarmSet = isAlarmSetSwitch.isChecked();
        Integer spinnerChoiceID = spinner.getSelectedItemPosition();
        int reminderID = Integer.valueOf(idField.getText().toString());
        long dateTime = calendar.getTimeInMillis();

        if (isTimePeriodValueSet(remindValueField)) {
            Integer remindBeforeDueDateValue = Integer.valueOf(remindValueField.getText().toString());

            final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(getApplicationContext());

            getChosenTimePeriod();

            final long alarmTriggerTime = calendar.getTimeInMillis();

            undoChosenTimePeriodCalendarChanges();

            long numOfRows = mydb.numberOfRows();

            int itemIndex = -1;

            ArrayList<ArrayList<String>> allRemindersData = mydb.getCompleteRemindersData();
            for (int i = 0; i < numOfRows; i++) {
                Integer currentRecordID = Integer.valueOf(allRemindersData.get(i).get(0));
                if (currentRecordID == reminderID) {
                    itemIndex = i;
                    break;
                }
            }

            if (numOfRows > 0 && itemIndex != -1) {
                mydb.updateReminder(reminderID, title, description, isImportant, isAlarmSet, dateTime, alarmTriggerTime, spinnerChoiceID, remindBeforeDueDateValue, isReminder);
            } else {
                // In other case just create new record in database
                mydb.insertReminder(title, description, isImportant, isAlarmSet, dateTime, alarmTriggerTime, spinnerChoiceID, remindBeforeDueDateValue, isReminder);
            }
            mydb.close();
        } else {
            ShowToastInIntentService(getString(R.string.toast_remind_before_due_date_value_missing));
        }
    }

    private void setAlarm(Switch alarmSwitch, Boolean isChecked, Boolean isTriggeredByAlarmSwitch) {
        final AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent myIntent = new Intent(getApplicationContext(), AlarmReceiver.class);

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String fullDate = sdf.format(calendar.getTimeInMillis());
//        String fullDate = calendar.get(Calendar.YEAR) + "." + calendar.get(Calendar.MONTH) + "." + calendar.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        String fullTime = simpleDateFormat.format(calendar.getTime());
        String alarmExtras = fullDate + getString(R.string.regex_separator) + fullTime;

        TextView reminderIdField = findViewById(R.id.singleReminderID);
        TextView reminderTitle = findViewById(R.id.singleReminderTitle);
        TextView reminderDescription = findViewById(R.id.singleReminderDescription);

        String intentExtras = reminderIdField.getText().toString() + getString(R.string.regex_separator) + reminderTitle.getText().toString() +
                getString(R.string.regex_separator) + reminderDescription.getText().toString() + getString(R.string.regex_separator) + alarmExtras;

        myIntent.putExtra(Intent.EXTRA_TEXT, intentExtras);

        final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(getApplicationContext());

        int reminderID = Integer.valueOf(reminderIdField.getText().toString());

        mydb.close();

        PendingIntent pendingIntent = PendingIntent.getBroadcast(ReminderItemViewMain.this, reminderID, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        EditText timePeriodValue = findViewById(R.id.remindBeforeDueDateValue);
        if (isTimePeriodValueSet(timePeriodValue)) {
            if (manager != null) {
                getChosenTimePeriod();
                if (isChecked && System.currentTimeMillis() < calendar.getTimeInMillis()) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                        manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                    else
                        manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                    undoChosenTimePeriodCalendarChanges();
                    saveCurrentRemindersData();
                    if (isTriggeredByAlarmSwitch)
                        ShowToastInIntentService(getString(R.string.toast_alarm_set));
                } else if (isChecked && System.currentTimeMillis() > calendar.getTimeInMillis()) {
                    undoChosenTimePeriodCalendarChanges();
                    alarmSwitch.setChecked(false);
                    if (isTriggeredByAlarmSwitch)
                        ShowToastInIntentService(getString(R.string.toast_alarm_not_set_wrong_datetime));
                } else if (!isChecked) {
                    manager.cancel(pendingIntent);
                    undoChosenTimePeriodCalendarChanges();
                    saveCurrentRemindersData();
                    if (isTriggeredByAlarmSwitch)
                        ShowToastInIntentService(getString(R.string.toast_alarm_unset));
                }
            } else {
                alarmSwitch.setChecked(false);
                if (isTriggeredByAlarmSwitch)
                    ShowToastInIntentService(getString(R.string.toast_alarm_service_unavailable));
            }
        } else {
            alarmSwitch.setChecked(false);
            if (isTriggeredByAlarmSwitch)
                ShowToastInIntentService(getString(R.string.toast_remind_before_due_date_value_missing));
        }
    }

    private void getChosenTimePeriod() {
        Spinner spinner = findViewById(R.id.timePeriodSpinner);
        long spinnerChoiceID = spinner.getSelectedItemId();
        EditText inputDatePeriodValue = findViewById(R.id.remindBeforeDueDateValue);
        if (spinnerChoiceID == 5)
            calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(inputDatePeriodValue.getText().toString()) * 30 * -1);
        if (spinnerChoiceID == 4)
            calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(inputDatePeriodValue.getText().toString()) * 7 * -1);
        if (spinnerChoiceID == 3)
            calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(inputDatePeriodValue.getText().toString()) * -1);
        if (spinnerChoiceID == 2)
            calendar.add(Calendar.HOUR_OF_DAY, Integer.valueOf(inputDatePeriodValue.getText().toString()) * -1);
        if (spinnerChoiceID == 1)
            calendar.add(Calendar.MINUTE, Integer.valueOf(inputDatePeriodValue.getText().toString()) * -1);
    }

    private void undoChosenTimePeriodCalendarChanges() {
        Spinner spinner = findViewById(R.id.timePeriodSpinner);
        long spinnerChoiceID = spinner.getSelectedItemId();
        EditText inputDatePeriodValue = findViewById(R.id.remindBeforeDueDateValue);
        if (spinnerChoiceID == 5)
            calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(inputDatePeriodValue.getText().toString()) * 30);
        if (spinnerChoiceID == 4)
            calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(inputDatePeriodValue.getText().toString()) * 7);
        if (spinnerChoiceID == 3)
            calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(inputDatePeriodValue.getText().toString()));
        if (spinnerChoiceID == 2)
            calendar.add(Calendar.HOUR_OF_DAY, Integer.valueOf(inputDatePeriodValue.getText().toString()));
        if (spinnerChoiceID == 1)
            calendar.add(Calendar.MINUTE, Integer.valueOf(inputDatePeriodValue.getText().toString()));
    }

    private boolean isTimePeriodValueSet(EditText fieldToCheck) {
        return !fieldToCheck.getText().toString().equals("");
    }

//    public void initChannels(Context context) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
//            return;
//        }
//        NotificationManager notificationManager =
//                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        NotificationChannel channel = new NotificationChannel("CHANNEL_ID",
//                "Personal Reminder",
//                NotificationManager.IMPORTANCE_DEFAULT);
//        channel.setDescription("Notification channel");
//        if (notificationManager != null)
//            notificationManager.createNotificationChannel(channel);
//    }

//    public int getNotificationID() {
//        return (int) System.currentTimeMillis();
//    }

    private boolean isInRange(int[] minMaxValueRange, int inputValue) {
        return (inputValue >= minMaxValueRange[0] && inputValue <= minMaxValueRange[1]);
    }

    private void setInputValueLimit(int[] inputRange, boolean isInRange, EditText inputField) {
        if (!isInRange) {
            Integer inputValue = Integer.valueOf(inputField.getText().toString());
            if (inputValue > inputRange[1])
                inputField.setText(String.valueOf(inputRange[1]));
            else if (inputValue < inputRange[0])
                inputField.setText(String.valueOf(inputRange[0]));
        }
        inputField.setFilters(new InputFilter[]{new InputFilterMinMax(inputRange[0], inputRange[1])});
    }

    @Override
    public void chosenSpinnerPeriod() {
        final Spinner spinner = findViewById(R.id.timePeriodSpinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EditText inputField = findViewById(R.id.remindBeforeDueDateValue);
                Integer inputValue;
                if (!inputField.getText().toString().equals("") && inputField.getText() != null) {
                    inputValue = Integer.valueOf(inputField.getText().toString());
                } else {
                    inputValue = 1;
                    inputField.setText(String.valueOf(inputValue));
                }
                int[] inputRange = new int[2];
                if (position == 5) {
                    inputRange = INPUT_LIMIT_MONTHS;
                    setSpinnerInputFocusability(inputField, true);
                } else if (position == 4) {
                    inputRange = INPUT_LIMIT_WEEKS;
                    setSpinnerInputFocusability(inputField, true);
                } else if (position == 3) {
                    inputRange = INPUT_LIMIT_DAYS;
                    setSpinnerInputFocusability(inputField, true);
                } else if (position == 2) {
                    inputRange = INPUT_LIMIT_HOURS;
                    setSpinnerInputFocusability(inputField, true);
                } else if (position == 1) {
                    inputRange = INPUT_LIMIT_MINUTES;
                    setSpinnerInputFocusability(inputField, true);
                } else {
                    inputRange[0] = 1;
                    inputRange[1] = 1;
                    setSpinnerInputFocusability(inputField, false);
                }
                setInputValueLimit(inputRange, isInRange(inputRange, inputValue), inputField);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void populateSpinner() {
        Spinner spinner = findViewById(R.id.timePeriodSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.time_period_spinner_values, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_reminder_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        final EditText inputField = findViewById(R.id.remindBeforeDueDateValue);

        initReminder();

        final EditText titleField = findViewById(R.id.singleReminderTitle);
        titleField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    titleField.setCursorVisible(true);
                } else {
                    titleField.setCursorVisible(false);
                    InputMethodManager softKeyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (softKeyboard != null) {
                        softKeyboard.hideSoftInputFromWindow(titleField.getWindowToken(), 0);
                    }
                }
            }
        });

        final EditText descriptionField = findViewById(R.id.singleReminderDescription);
        descriptionField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    descriptionField.setCursorVisible(true);
                } else {
                    descriptionField.setCursorVisible(false);
                    InputMethodManager softKeyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (softKeyboard != null) {
                        softKeyboard.hideSoftInputFromWindow(descriptionField.getWindowToken(), 0);
                    }
                }
            }
        });

        inputField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    inputField.setCursorVisible(true);
                } else {
                    inputField.setCursorVisible(false);
                    InputMethodManager softKeyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (softKeyboard != null) {
                        softKeyboard.hideSoftInputFromWindow(inputField.getWindowToken(), 0);
                    }
                }
            }
        });

        if (inputField.getText().toString().equals(""))
            inputField.setText(getString(R.string.input_init_default_value));

        TextView initDateField = findViewById(R.id.datepickerValue);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String fullDate = sdf.format(calendar.getTimeInMillis());
        initDateField.setText(fullDate);

        final Button popupDatePicker = findViewById(R.id.buttonSetDate);
        popupDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(ReminderItemViewMain.this);
                LinearLayout reminderContainerLinearLayout = findViewById(R.id.reminderContainer);

                View customView = inflater.inflate(R.layout.datepicker, reminderContainerLinearLayout, false);

                mPopupWindow = new PopupWindow(
                        customView,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                );

                // Set an elevation value for popup window
                // Call requires API level 21
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    popupDatePicker.setElevation(5.0f);
                }

                DatePicker datePicker = customView.findViewById(R.id.datepicker);
                datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

                    @Override
                    public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                        TextView dateField = findViewById(R.id.datepickerValue);

                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                        String fullDate = sdf.format(calendar.getTimeInMillis());
                        dateField.setText(fullDate);
                    }
                });

                View closeArea = customView.findViewById(R.id.datepickerBody);

                closeArea.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPopupWindow.dismiss();
                    }
                });

                mPopupWindow.showAtLocation(findViewById(R.id.reminderContainer), Gravity.CENTER, Gravity.CENTER_HORIZONTAL, Gravity.CENTER_VERTICAL);
            }
        });

        TextView initTimeField = findViewById(R.id.timepickerValue);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        initTimeField.setText(simpleDateFormat.format(calendar.getTime()));

        final Button popupTimePicker = findViewById(R.id.buttonSetTime);
        popupTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(ReminderItemViewMain.this);
                LinearLayout reminderContainerLinearLayout = findViewById(R.id.reminderContainer);

                final View customView = inflater.inflate(R.layout.timepicker, reminderContainerLinearLayout, false);

                mPopupWindow = new PopupWindow(
                        customView,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                );

                // Set an elevation value for popup window
                // Call requires API level 21
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    popupTimePicker.setElevation(5.0f);
                }

                TimePicker timePicker = customView.findViewById(R.id.timepicker);
                timePicker.setIs24HourView(true);

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
                    timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));
                } else {
                    timePicker.setHour(calendar.get(Calendar.HOUR_OF_DAY));
                    timePicker.setMinute(calendar.get(Calendar.MINUTE));
                }

                timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                    @Override
                    public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                        TextView timeField = findViewById(R.id.timepickerValue);
                        calendar.set(Calendar.HOUR_OF_DAY, i);
                        calendar.set(Calendar.MINUTE, i1);
                        calendar.set(Calendar.SECOND, 0);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                        timeField.setText(simpleDateFormat.format(calendar.getTime()));
                    }
                });

                View closeArea = customView.findViewById(R.id.timepickerBody);

                closeArea.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPopupWindow.dismiss();
                    }
                });

                mPopupWindow.showAtLocation(findViewById(R.id.reminderContainer), Gravity.CENTER, Gravity.CENTER_HORIZONTAL, Gravity.CENTER_VERTICAL);
            }
        });

        populateSpinner();

        chosenSpinnerPeriod();

        final LinearLayout reminderSettingsContainer = findViewById(R.id.reminderSettingsContainer);

        final Switch alarmSwitch = findViewById(R.id.toggleAlarm);
        final Switch isNoteOrReminderSwitch = findViewById(R.id.isNoteOrReminder);
        isNoteOrReminderSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked) {
                    saveCurrentRemindersData();
                    reminderSettingsContainer.setVisibility(View.VISIBLE);
                } else {
                    setAlarm(alarmSwitch, isChecked, false);
                    reminderSettingsContainer.setVisibility(View.INVISIBLE);
                }
            }
        });

        alarmSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isNoteOrReminderSwitch.isChecked())
                    setAlarm(alarmSwitch, isChecked, true);
            }
        });

        Button prev = findViewById(R.id.buttonPrev);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Button save = findViewById(R.id.buttonCreate);
        save.setText(R.string.button_save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText periodTimeField = findViewById(R.id.remindBeforeDueDateValue);
                Switch alarmSwitch = findViewById(R.id.toggleAlarm);
                if (titleField.getText().toString().matches("")) {
                    ShowToastInIntentService(getString(R.string.error_reminder_no_title_set));
                    return;
                } else if (descriptionField.getText().toString().matches("")) {
                    ShowToastInIntentService(getString(R.string.error_reminder_no_description_set));
                    return;
                } else {
                    if (isTimePeriodValueSet(periodTimeField)) {
                        // setAlarm function is triggered instead of data save function due to its proper occurrences handling
                        // implemented for its inner saveCurrentRemindersData function
                        setAlarm(alarmSwitch, alarmSwitch.isChecked(), false);
                        ShowToastInIntentService(getString(R.string.toast_reminder_database_updated));
                        finish();
                    } else {
                        ShowToastInIntentService(getString(R.string.toast_remind_before_due_date_value_missing));
                    }
                }
            }
        });
    }
}
