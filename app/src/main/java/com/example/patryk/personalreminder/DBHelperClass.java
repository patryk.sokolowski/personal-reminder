package com.example.patryk.personalreminder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class DBHelperClass {
    public static class DBHelper extends SQLiteOpenHelper {

        static final String DATABASE_NAME = "remindersDatabase.db";
        static final String REMINDERS_TABLE_NAME = "reminders";
        static final String REMINDERS_COLUMN_ID = "id";
        static final String REMINDERS_COLUMN_TITLE = "title";
        static final String REMINDERS_COLUMN_DESCRIPTION = "description";
        static final String REMINDERS_COLUMN_IS_IMPORTANT = "isImportant";
        static final String REMINDERS_COLUMN_IS_ALARM_SET = "isAlarmSet";
        static final String REMINDERS_COLUMN_DATETIME = "dateTime";
        static final String REMINDERS_COLUMN_ALARM_TRIGGER_ON_TIME = "alarmTriggerTime";
        static final String REMINDERS_COLUMN_CHOSEN_SPINNER_PERIOD = "spinnerChoiceID";
        static final String REMINDERS_COLUMN_TIME_INPUT_VALUE = "timeBeforeReminderTrigger";
        static final String REMINDERS_COLUMN_IS_REMINDER = "isReminder";
        HashMap hp;

        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(
                    "create table " + REMINDERS_TABLE_NAME +
                            " ( " + REMINDERS_COLUMN_ID + " long primary key, " + REMINDERS_COLUMN_TITLE + " text, " + REMINDERS_COLUMN_DESCRIPTION + " text," +
                            REMINDERS_COLUMN_IS_IMPORTANT + " boolean, " + REMINDERS_COLUMN_IS_ALARM_SET + " boolean, " +
                            REMINDERS_COLUMN_DATETIME + " long, " + REMINDERS_COLUMN_ALARM_TRIGGER_ON_TIME + " long," + REMINDERS_COLUMN_CHOSEN_SPINNER_PERIOD + " integer, " +
                            REMINDERS_COLUMN_TIME_INPUT_VALUE + " integer," + REMINDERS_COLUMN_IS_REMINDER + " boolean)"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            String deletionQuery = "DROP TABLE IF EXISTS " + REMINDERS_TABLE_NAME;
            db.execSQL(deletionQuery);
            onCreate(db);
        }

        public boolean insertReminder(String title, String description, Boolean isImportant, Boolean isAlarmSet, long dateTime, long alarmTriggerTime, int spinnerChoiceID, int timeBeforeTrigger, Boolean isReminder) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(REMINDERS_COLUMN_TITLE, title);
            contentValues.put(REMINDERS_COLUMN_DESCRIPTION, description);
            contentValues.put(REMINDERS_COLUMN_IS_IMPORTANT, isImportant);
            contentValues.put(REMINDERS_COLUMN_IS_ALARM_SET, isAlarmSet);
            contentValues.put(REMINDERS_COLUMN_DATETIME, dateTime);
            contentValues.put(REMINDERS_COLUMN_ALARM_TRIGGER_ON_TIME, alarmTriggerTime);
            contentValues.put(REMINDERS_COLUMN_CHOSEN_SPINNER_PERIOD, spinnerChoiceID);
            contentValues.put(REMINDERS_COLUMN_TIME_INPUT_VALUE, timeBeforeTrigger);
            contentValues.put(REMINDERS_COLUMN_IS_REMINDER, isReminder);
            db.insert(REMINDERS_TABLE_NAME, null, contentValues);
            return true;
        }

        public Cursor getDataCursor(long id) {
            SQLiteDatabase db = this.getReadableDatabase();
//            return db.rawQuery( "SELECT * FROM " + REMINDERS_TABLE_NAME + " WHERE id="+id+"", null );
            return db.rawQuery("SELECT * FROM " + REMINDERS_TABLE_NAME + " WHERE id="+id+" ORDER BY " + REMINDERS_COLUMN_DATETIME, null);
        }

        public ArrayList<String> getReminderData(long id) {
            Cursor dataCursor = getDataCursor(id);
            ArrayList<String> fetchedRow = new ArrayList<>();
            dataCursor.moveToFirst();
            for (int i = 0; i < dataCursor.getColumnCount(); i++) {
                String fetchedColumn = dataCursor.getString(dataCursor.getColumnIndex(dataCursor.getColumnName(i)));
                fetchedRow.add(fetchedColumn);
            }
            return fetchedRow;
        }

        public long numberOfRows() {
            SQLiteDatabase db = this.getReadableDatabase();
            return DatabaseUtils.queryNumEntries(db, REMINDERS_TABLE_NAME);
        }

        public boolean updateReminder(long id, String title, String description, Boolean isImportant, Boolean isAlarmSet, long dateTime, long alarmTriggerTime, int spinnerChoiceID, int timeBeforeTrigger, Boolean isReminder) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(REMINDERS_COLUMN_TITLE, title);
            contentValues.put(REMINDERS_COLUMN_DESCRIPTION, description);
            contentValues.put(REMINDERS_COLUMN_IS_IMPORTANT, isImportant);
            contentValues.put(REMINDERS_COLUMN_IS_ALARM_SET, isAlarmSet);
            contentValues.put(REMINDERS_COLUMN_DATETIME, dateTime);
            contentValues.put(REMINDERS_COLUMN_ALARM_TRIGGER_ON_TIME, alarmTriggerTime);
            contentValues.put(REMINDERS_COLUMN_CHOSEN_SPINNER_PERIOD, spinnerChoiceID);
            contentValues.put(REMINDERS_COLUMN_TIME_INPUT_VALUE, timeBeforeTrigger);
            contentValues.put(REMINDERS_COLUMN_IS_REMINDER, isReminder);
            db.update(REMINDERS_TABLE_NAME, contentValues, "id = ? ", new String[]{Long.toString(id)});
            return true;
        }

        public Integer deleteReminder(long id) {
            SQLiteDatabase db = this.getWritableDatabase();
            return db.delete(REMINDERS_TABLE_NAME,
                    "id = ? ",
                    new String[]{Long.toString(id)});
        }

        public ArrayList<String> getRemindersTitlesList() {
            ArrayList<String> array_list = new ArrayList<String>();

            //hp = new HashMap();
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor res = db.rawQuery("SELECT " + REMINDERS_COLUMN_TITLE + " FROM " + REMINDERS_TABLE_NAME, null);
            res.moveToFirst();

            while (!res.isAfterLast()) {
                array_list.add(res.getString(res.getColumnIndex(REMINDERS_COLUMN_TITLE)));
                res.moveToNext();
            }
            return array_list;
        }

        public ArrayList<String> getAllReminders() {
            ArrayList<String> array_list = new ArrayList<String>();

            //hp = new HashMap();
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor res = db.rawQuery("SELECT * FROM " + REMINDERS_TABLE_NAME, null);
            res.moveToFirst();

            while (!res.isAfterLast()) {
                array_list.add(res.getString(res.getColumnIndex(REMINDERS_COLUMN_TITLE)));
                res.moveToNext();
            }
            return array_list;
        }

        public ArrayList<ArrayList<String>> getCompleteRemindersData() {
            ArrayList<ArrayList<String>> array_list = new ArrayList<>();

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor dataCursor = db.rawQuery("SELECT * FROM " + REMINDERS_TABLE_NAME + " ORDER BY " + REMINDERS_COLUMN_DATETIME, null);
            dataCursor.moveToFirst();
            Integer numberOfColumns = dataCursor.getColumnCount();

            int i = 0;
            while (!dataCursor.isAfterLast()) {
                array_list.add(new ArrayList<String>());
                for (int j = 0; j < numberOfColumns; j++)
                    array_list.get(i).add(dataCursor.getString(dataCursor.getColumnIndex(dataCursor.getColumnName(j))));
                dataCursor.moveToNext();
                i++;
            }

            return array_list;
        }
    }
}
