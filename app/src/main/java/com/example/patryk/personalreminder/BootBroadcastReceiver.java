package com.example.patryk.personalreminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class BootBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Calendar calendar = Calendar.getInstance();

        final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(context);

        ArrayList<ArrayList<String>> remindersData = mydb.getCompleteRemindersData();

        final AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        for (int i = 0; i < mydb.numberOfRows(); i++) {
            if (Integer.valueOf(remindersData.get(i).get(4)) == 1) {
                Intent myIntent = new Intent(context, AlarmReceiver.class);
                int reminderID = Integer.valueOf(remindersData.get(i).get(0));
                long reminderDueDate = Long.valueOf(remindersData.get(i).get(5));
                long alarmTriggerDate = Long.valueOf(remindersData.get(i).get(6));
                calendar.setTimeInMillis(reminderDueDate);
                String remindersTitle = remindersData.get(i).get(1);
                String remindersDescription = remindersData.get(i).get(2);
                String fullDate = calendar.get(Calendar.YEAR) + context.getString(R.string.dot_separator) +
                        calendar.get(Calendar.MONTH) + context.getString(R.string.dot_separator) + calendar.get(Calendar.DAY_OF_MONTH);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                String fullTime = simpleDateFormat.format(calendar.getTime());
                String intentExtras = reminderID + context.getString(R.string.regex_separator) + remindersTitle + context.getString(R.string.regex_separator) +
                        remindersDescription + context.getString(R.string.regex_separator) + fullDate + context.getString(R.string.regex_separator) + fullTime;
                myIntent.putExtra(Intent.EXTRA_TEXT, intentExtras);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, i, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                if (manager != null && System.currentTimeMillis() < alarmTriggerDate) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                        manager.setExact(AlarmManager.RTC_WAKEUP, alarmTriggerDate, pendingIntent);
                    else
                        manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTriggerDate, pendingIntent);
                }
            }
        }
        mydb.close();
    }
}
