package com.example.patryk.personalreminder;

/**
 * Created by Patryk Sokołowski on 09.05.18.
 */

public class Constants {
    public static final long DEFAULT_INIT_VALUE_LONG = -1;
    public static final int DEFAULT_INIT_VALUE_INT = -1;
}
