package com.example.patryk.personalreminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Patryk Sokołowski on 2018-03-19.
 */

public class AlarmViewMain extends AppCompatActivity {
    public static int SNOOZE_TIME_ONE_MINUTE = 1000 * 60;
    private Calendar calendar = Calendar.getInstance();

    public void ShowToastInIntentService(final String sText) {
        final Context MyContext = getApplicationContext().getApplicationContext();

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MyContext, sText, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public Boolean intToBoolean(int value) {
        return value > 0;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        final Context context = getApplicationContext().getApplicationContext();

        final PowerManager pwrmanager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        final PowerManager.WakeLock wakelock;
        wakelock = pwrmanager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, BuildConfig.APPLICATION_ID);
        wakelock.acquire(1000 * 60 * 5);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final String prefSnoozeTimeMinutes = preferences.getString("alarm_snooze_time_value", "5");

        // Start vibrations when Alarm is triggered
        final Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        if (vibrator != null)
            vibrator.vibrate(new long[]{0, 400, 1000}, 0);

        // Play sound when Alarm is triggered
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (notification == null) {
            notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (notification == null)
                notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        }
        final Ringtone ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
        ringtone.play();
//        final MediaPlayer mp = MediaPlayer.create(getApplicationContext(), notification);
//        mp.start();

        // Wake device when Alarm is triggered
        Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        String intentExtras = getIntent().getStringExtra(Intent.EXTRA_TEXT);

        final String[] reminderContent = intentExtras.split(getString(R.string.regex_separator));
        final TextView thisReminderTitle = findViewById(R.id.alarm_reminder_title);
        final TextView thisReminderDescription = findViewById(R.id.alarm_reminder_description);

        thisReminderTitle.setText(reminderContent[1]);
        thisReminderDescription.setText(reminderContent[2]);

        TextView thisScheduleTime = findViewById(R.id.alarm_reminder_schedule_time);
        TextView thisScheduleDate = findViewById(R.id.alarm_reminder_schedule_date);

        thisScheduleDate.setText(reminderContent[3]);
        thisScheduleTime.setText(reminderContent[4]);

        final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(getApplicationContext());

        ArrayList<String> reminderData = mydb.getReminderData(Integer.valueOf(reminderContent[0]));
        mydb.updateReminder(
                Integer.valueOf(reminderData.get(0)),
                reminderData.get(1),
                reminderData.get(2),
                intToBoolean(Integer.valueOf(reminderData.get(3))),
                false,
                Long.valueOf(reminderData.get(5)),
                Long.valueOf(reminderData.get(6)),
                Integer.valueOf(reminderData.get(7)),
                Integer.valueOf(reminderData.get(8)),
                intToBoolean(Integer.valueOf(reminderData.get(9)))
        );

        mydb.close();

        Button rescheduleButton = findViewById(R.id.alarm_control_reschedule);
        rescheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getApplicationContext(), ReminderItemViewMain.class);
                String intentExtras = reminderContent[0];
                myIntent.putExtra(Intent.EXTRA_TEXT, intentExtras);
                if (vibrator != null)
                    vibrator.cancel();
                ringtone.stop();
                if (wakelock.isHeld())
                    wakelock.release();
                finish();
                startActivity(myIntent);
            }
        });

        final AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Button snoozeButton = findViewById(R.id.alarm_control_snooze);
        snoozeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getApplicationContext(), AlarmReceiver.class);
                String intentExtras = getIntent().getStringExtra(Intent.EXTRA_TEXT);

                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                String fullDate = sdf.format(calendar.getTimeInMillis());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                String fullTime = simpleDateFormat.format(calendar.getTime());

                long newTimeWithSnooze = calendar.getTimeInMillis() + SNOOZE_TIME_ONE_MINUTE * Long.valueOf(prefSnoozeTimeMinutes);
                calendar.setTimeInMillis(newTimeWithSnooze);

                String alarmExtras = fullDate + getString(R.string.regex_separator) + fullTime;

                intentExtras += getString(R.string.regex_separator) + alarmExtras;

                myIntent.putExtra(Intent.EXTRA_TEXT, intentExtras);

                final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(getApplicationContext());

                ArrayList<String> reminderData = mydb.getReminderData(Integer.valueOf(reminderContent[0]));
                mydb.updateReminder(
                        Integer.valueOf(reminderData.get(0)),
                        reminderData.get(1),
                        reminderData.get(2),
                        intToBoolean(Integer.valueOf(reminderData.get(3))),
                        true,
                        Long.valueOf(reminderData.get(5)),
                        Long.valueOf(reminderData.get(6)),
                        Integer.valueOf(reminderData.get(7)),
                        Integer.valueOf(reminderData.get(8)),
                        intToBoolean(Integer.valueOf(reminderData.get(9)))
                );

                mydb.close();

                Integer requestCode = Integer.valueOf(reminderContent[0]);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), requestCode, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                if (manager != null)
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                        manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                    else
                        manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                if (vibrator != null)
                    vibrator.cancel();
                ringtone.stop();
//                mp.stop();
                if (wakelock.isHeld())
                    wakelock.release();
                finish();

                ShowToastInIntentService(getString(R.string.alarm_snooze_text_before_snooze_value) + " " + String.valueOf(prefSnoozeTimeMinutes) + " " + getString(R.string.alarm_snooze_text_after_snooze_value));
            }
        });

        Button turnOffButton = findViewById(R.id.alarm_control_turnoff);
        turnOffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vibrator != null)
                    vibrator.cancel();
                ringtone.stop();
//                mp.stop();
                if (wakelock.isHeld())
                    wakelock.release();
                finish();
            }
        });
    }
}
