package com.example.patryk.personalreminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Patryk Sokołowski on 01.03.18.
 */

public class ReminderCreatorMain extends AppCompatActivity implements Reminder {
    final static long DEFAULT_INIT_VALUE_LONG = -1;
    final static int DEFAULT_INIT_VALUE_INT = -1;
    final static int[] INPUT_LIMIT_MONTHS = {1, 11};
    final static int[] INPUT_LIMIT_WEEKS = {1, 3};
    final static int[] INPUT_LIMIT_DAYS = {1, 6};
    final static int[] INPUT_LIMIT_HOURS = {1, 23};
    final static int[] INPUT_LIMIT_MINUTES = {1, 59};
    private Calendar calendar = Calendar.getInstance();
    private PopupWindow mPopupWindow;

    private boolean writeToDatabase(String title, String description) {
        final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(getApplicationContext());

        Boolean result = mydb.insertReminder(title, description, false, false, System.currentTimeMillis(),
                DEFAULT_INIT_VALUE_LONG, DEFAULT_INIT_VALUE_INT, DEFAULT_INIT_VALUE_INT, false);

        mydb.close();
        return result;
    }

    public void ShowToastInIntentService(final String sText) {
        final Context MyContext = ReminderCreatorMain.this;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MyContext, sText, Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean isTimePeriodValueSet(EditText fieldToCheck) {
        return !fieldToCheck.getText().toString().equals("");
    }

    private void alarmValidator(Switch alarmSwitch, Boolean isChecked, Boolean isTriggeredByAlarmSwitch) {
        final AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        EditText timePeriodValue = findViewById(R.id.remindBeforeDueDateValue);

        if (isTimePeriodValueSet(timePeriodValue)) {
            if (manager != null) {
                getChosenTimePeriod();
                if (isChecked && System.currentTimeMillis() > calendar.getTimeInMillis()) {
                    alarmSwitch.setChecked(false);
                    if (isTriggeredByAlarmSwitch)
                        ShowToastInIntentService(getString(R.string.toast_alarm_not_set_wrong_datetime));
                }
                undoChosenTimePeriodCalendarChanges();
            } else {
                alarmSwitch.setChecked(false);
                if (isTriggeredByAlarmSwitch)
                    ShowToastInIntentService(getString(R.string.toast_alarm_service_unavailable));
            }
        } else {
            alarmSwitch.setChecked(false);
            if (isTriggeredByAlarmSwitch)
                ShowToastInIntentService(getString(R.string.toast_remind_before_due_date_value_missing));
        }
    }

    private void setAlarm(Switch alarmSwitch, Boolean isChecked, Boolean isTriggeredByAlarmSwitch) {
        final AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent myIntent = new Intent(getApplicationContext(), AlarmReceiver.class);

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String fullDate = sdf.format(calendar.getTimeInMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        String fullTime = simpleDateFormat.format(calendar.getTime());
        String alarmExtras = fullDate + getString(R.string.regex_separator) + fullTime;

        TextView reminderTitle = findViewById(R.id.reminderTitle);
        TextView reminderDescription = findViewById(R.id.reminderDescription);

        final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(getApplicationContext());

        ArrayList<ArrayList<String>> remindersList = mydb.getCompleteRemindersData();
        int newReminderID = -1;
        for (int i = 0; i < mydb.numberOfRows(); i++) {
            int currentReminderID = Integer.valueOf(remindersList.get(i).get(0));
            if (currentReminderID > newReminderID)
                newReminderID = currentReminderID;
        }

        mydb.close();

        String intentExtras = String.valueOf(newReminderID) + getString(R.string.regex_separator) + reminderTitle.getText().toString() +
                getString(R.string.regex_separator) + reminderDescription.getText().toString() + getString(R.string.regex_separator) + alarmExtras;

        myIntent.putExtra(Intent.EXTRA_TEXT, intentExtras);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), newReminderID, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        EditText timePeriodValue = findViewById(R.id.remindBeforeDueDateValue);
        if (isTimePeriodValueSet(timePeriodValue)) {
            if (manager != null) {
                getChosenTimePeriod();
                if (isChecked && System.currentTimeMillis() < calendar.getTimeInMillis()) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                        manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                    else
                        manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                    undoChosenTimePeriodCalendarChanges();
                    if (isTriggeredByAlarmSwitch)
                        ShowToastInIntentService(getString(R.string.toast_alarm_set));
                } else if (isChecked && System.currentTimeMillis() > calendar.getTimeInMillis()) {
                    undoChosenTimePeriodCalendarChanges();
                    alarmSwitch.setChecked(false);
                    if (isTriggeredByAlarmSwitch)
                        ShowToastInIntentService(getString(R.string.toast_alarm_not_set_wrong_datetime));
                } else if (!isChecked) {
                    manager.cancel(pendingIntent);
                    undoChosenTimePeriodCalendarChanges();
                    if (isTriggeredByAlarmSwitch)
                        ShowToastInIntentService(getString(R.string.toast_alarm_unset));
                }
            } else {
                alarmSwitch.setChecked(false);
                if (isTriggeredByAlarmSwitch)
                    ShowToastInIntentService(getString(R.string.toast_alarm_service_unavailable));
            }
        } else {
            alarmSwitch.setChecked(false);
            if (isTriggeredByAlarmSwitch)
                ShowToastInIntentService(getString(R.string.toast_remind_before_due_date_value_missing));
        }
    }

    private void getChosenTimePeriod() {
        Spinner spinner = findViewById(R.id.timePeriodSpinner);
        long spinnerChoiceID = spinner.getSelectedItemId();
        EditText inputDatePeriodValue = findViewById(R.id.remindBeforeDueDateValue);
        if (spinnerChoiceID == 5)
            calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(inputDatePeriodValue.getText().toString()) * 30 * -1);
        if (spinnerChoiceID == 4)
            calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(inputDatePeriodValue.getText().toString()) * 7 * -1);
        if (spinnerChoiceID == 3)
            calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(inputDatePeriodValue.getText().toString()) * -1);
        if (spinnerChoiceID == 2)
            calendar.add(Calendar.HOUR_OF_DAY, Integer.valueOf(inputDatePeriodValue.getText().toString()) * -1);
        if (spinnerChoiceID == 1)
            calendar.add(Calendar.MINUTE, Integer.valueOf(inputDatePeriodValue.getText().toString()) * -1);
    }

    private void undoChosenTimePeriodCalendarChanges() {
        Spinner spinner = findViewById(R.id.timePeriodSpinner);
        long spinnerChoiceID = spinner.getSelectedItemId();
        EditText inputDatePeriodValue = findViewById(R.id.remindBeforeDueDateValue);
        if (spinnerChoiceID == 5)
            calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(inputDatePeriodValue.getText().toString()) * 30);
        if (spinnerChoiceID == 4)
            calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(inputDatePeriodValue.getText().toString()) * 7);
        if (spinnerChoiceID == 3)
            calendar.add(Calendar.DAY_OF_YEAR, Integer.valueOf(inputDatePeriodValue.getText().toString()));
        if (spinnerChoiceID == 2)
            calendar.add(Calendar.HOUR_OF_DAY, Integer.valueOf(inputDatePeriodValue.getText().toString()));
        if (spinnerChoiceID == 1)
            calendar.add(Calendar.MINUTE, Integer.valueOf(inputDatePeriodValue.getText().toString()));
    }

    private boolean saveCurrentRemindersData() {
        TextView titleField = findViewById(R.id.reminderTitle);
        TextView descriptionField = findViewById(R.id.reminderDescription);
        CheckBox isImportantField = findViewById(R.id.checkboxIsReminderImportant);
        Switch isNoteOrReminderSwitch = findViewById(R.id.isNoteOrReminder);
        Switch isAlarmSetSwitch = findViewById(R.id.toggleAlarm);
        Spinner spinner = findViewById(R.id.timePeriodSpinner);
        EditText remindValueField = findViewById(R.id.remindBeforeDueDateValue);
        String title = titleField.getText().toString();
        String description = descriptionField.getText().toString();
        Boolean isReminder = isNoteOrReminderSwitch.isChecked();
        Boolean isImportant = isImportantField.isChecked();

        if (!isReminder)
            isAlarmSetSwitch.setChecked(isReminder);

        Boolean isAlarmSet = isAlarmSetSwitch.isChecked();
        Integer spinnerChoiceID = spinner.getSelectedItemPosition();
        long dateTime = calendar.getTimeInMillis();

        Boolean result = false;

        if (isTimePeriodValueSet(remindValueField)) {
            Integer remindBeforeDueDateValue = Integer.valueOf(remindValueField.getText().toString());

            final DBHelperClass.DBHelper mydb = new DBHelperClass.DBHelper(getApplicationContext());

            getChosenTimePeriod();

            final long alarmTriggerTime = calendar.getTimeInMillis();

            undoChosenTimePeriodCalendarChanges();

            result = mydb.insertReminder(title, description, isImportant, isAlarmSet, dateTime, alarmTriggerTime, spinnerChoiceID, remindBeforeDueDateValue, isReminder);

            mydb.close();
        } else {
            ShowToastInIntentService(getString(R.string.toast_remind_before_due_date_value_missing));
        }

        return result;
    }

    public void setSpinnerInputFocusability(EditText inputField, Boolean isEnabled) {
        inputField.setFocusableInTouchMode(isEnabled);
        inputField.setFocusable(isEnabled);
        inputField.setEnabled(isEnabled);
    }

    private void setInputValueLimit(int[] inputRange, boolean isInRange, EditText inputField) {
        if (!isInRange) {
            Integer inputValue = Integer.valueOf(inputField.getText().toString());
            if (inputValue > inputRange[1])
                inputField.setText(String.valueOf(inputRange[1]));
            else if (inputValue < inputRange[0])
                inputField.setText(String.valueOf(inputRange[0]));
        }
        inputField.setFilters(new InputFilter[]{new InputFilterMinMax(inputRange[0], inputRange[1])});
    }

    private boolean isInRange(int[] minMaxValueRange, int inputValue) {
        return (inputValue >= minMaxValueRange[0] && inputValue <= minMaxValueRange[1]);
    }

    @Override
    public void chosenSpinnerPeriod() {
        final Spinner spinner = findViewById(R.id.timePeriodSpinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EditText inputField = findViewById(R.id.remindBeforeDueDateValue);
                Integer inputValue;
                if (!inputField.getText().toString().equals("") && inputField.getText() != null) {
                    inputValue = Integer.valueOf(inputField.getText().toString());
                } else {
                    inputValue = 1;
                    inputField.setText(String.valueOf(inputValue));
                }
                int[] inputRange = new int[2];
                if (position == 5) {
                    inputRange = INPUT_LIMIT_MONTHS;
                    setSpinnerInputFocusability(inputField, true);
                } else if (position == 4) {
                    inputRange = INPUT_LIMIT_WEEKS;
                    setSpinnerInputFocusability(inputField, true);
                } else if (position == 3) {
                    inputRange = INPUT_LIMIT_DAYS;
                    setSpinnerInputFocusability(inputField, true);
                } else if (position == 2) {
                    inputRange = INPUT_LIMIT_HOURS;
                    setSpinnerInputFocusability(inputField, true);
                } else if (position == 1) {
                    inputRange = INPUT_LIMIT_MINUTES;
                    setSpinnerInputFocusability(inputField, true);
                } else {
                    inputRange[0] = 1;
                    inputRange[1] = 1;
                    setSpinnerInputFocusability(inputField, false);
                }
                setInputValueLimit(inputRange, isInRange(inputRange, inputValue), inputField);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void populateSpinner() {
        Spinner spinner = findViewById(R.id.timePeriodSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.time_period_spinner_values, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void initView() {
        EditText inputField = findViewById(R.id.remindBeforeDueDateValue);

        refreshViewVisibility();

        if (inputField.getText().toString().equals(""))
            inputField.setText(getString(R.string.input_init_default_value));

        TextView initDateField = findViewById(R.id.datepickerValue);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String fullDate = sdf.format(calendar.getTimeInMillis());
        initDateField.setText(fullDate);

        TextView initTimeField = findViewById(R.id.timepickerValue);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        initTimeField.setText(simpleDateFormat.format(calendar.getTime()));
    }

    private void refreshViewVisibility() {
        LinearLayout reminderSettingsContainer = findViewById(R.id.reminderSettingsContainer);
        Switch isNoteOrReminder = findViewById(R.id.isNoteOrReminder);
        if (isNoteOrReminder.isChecked())
            reminderSettingsContainer.setVisibility(View.VISIBLE);
        else
            reminderSettingsContainer.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creator_reminder_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initView();

        EditText reminderTitle = findViewById(R.id.reminderTitle);
        EditText reminderDescription = findViewById(R.id.reminderDescription);
        Switch isNoteOrReminder = findViewById(R.id.isNoteOrReminder);
        Switch isAlarmSet = findViewById(R.id.toggleAlarm);
        final EditText inputField = findViewById(R.id.remindBeforeDueDateValue);
        final LinearLayout reminderSettingsContainer = findViewById(R.id.reminderSettingsContainer);

        final Button popupDatePicker = findViewById(R.id.buttonSetDate);
        popupDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(ReminderCreatorMain.this);
                LinearLayout reminderContainerLinearLayout = findViewById(R.id.reminderCreatorContainer);

                View customView = inflater.inflate(R.layout.datepicker, reminderContainerLinearLayout, false);

                mPopupWindow = new PopupWindow(
                        customView,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                );

                // Set an elevation value for popup window
                // Call requires API level 21
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    popupDatePicker.setElevation(5.0f);
                }

                DatePicker datePicker = customView.findViewById(R.id.datepicker);
                datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

                    @Override
                    public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                        TextView dateField = findViewById(R.id.datepickerValue);

                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                        String fullDate = sdf.format(calendar.getTimeInMillis());
                        dateField.setText(fullDate);
                    }
                });

                View closeArea = customView.findViewById(R.id.datepickerBody);

                closeArea.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPopupWindow.dismiss();
                    }
                });

                mPopupWindow.showAtLocation(findViewById(R.id.reminderCreatorContainer), Gravity.CENTER, Gravity.CENTER_HORIZONTAL, Gravity.CENTER_VERTICAL);
            }
        });

        TextView initTimeField = findViewById(R.id.timepickerValue);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        initTimeField.setText(simpleDateFormat.format(calendar.getTime()));

        final Button popupTimePicker = findViewById(R.id.buttonSetTime);
        popupTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(ReminderCreatorMain.this);
                LinearLayout reminderContainerLinearLayout = findViewById(R.id.reminderCreatorContainer);

                final View customView = inflater.inflate(R.layout.timepicker, reminderContainerLinearLayout, false);

                mPopupWindow = new PopupWindow(
                        customView,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                );

                // Set an elevation value for popup window
                // Call requires API level 21
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    popupTimePicker.setElevation(5.0f);
                }

                TimePicker timePicker = customView.findViewById(R.id.timepicker);
                timePicker.setIs24HourView(true);

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
                    timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));
                } else {
                    timePicker.setHour(calendar.get(Calendar.HOUR_OF_DAY));
                    timePicker.setMinute(calendar.get(Calendar.MINUTE));
                }

                timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                    @Override
                    public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                        TextView timeField = findViewById(R.id.timepickerValue);
                        calendar.set(Calendar.HOUR_OF_DAY, i);
                        calendar.set(Calendar.MINUTE, i1);
                        calendar.set(Calendar.SECOND, 0);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                        timeField.setText(simpleDateFormat.format(calendar.getTime()));
                    }
                });

                View closeArea = customView.findViewById(R.id.timepickerBody);

                closeArea.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPopupWindow.dismiss();
                    }
                });

                mPopupWindow.showAtLocation(findViewById(R.id.reminderCreatorContainer), Gravity.CENTER, Gravity.CENTER_HORIZONTAL, Gravity.CENTER_VERTICAL);
            }
        });

        populateSpinner();
        chosenSpinnerPeriod();

        isNoteOrReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Switch alarmSwitch = findViewById(R.id.toggleAlarm);
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked) {
                    reminderSettingsContainer.setVisibility(View.VISIBLE);
                } else {
                    alarmValidator(alarmSwitch, isChecked, false);
                    reminderSettingsContainer.setVisibility(View.INVISIBLE);
                }
            }
        });

        isAlarmSet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Switch isNoteOrReminderSwitch = findViewById(R.id.isNoteOrReminder);
                Switch alarmSwitch = findViewById(R.id.toggleAlarm);
                if (isNoteOrReminderSwitch.isChecked())
                    alarmValidator(alarmSwitch, isChecked, true);
            }
        });

        reminderTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                EditText reminderTitle = v.findViewById(R.id.reminderTitle);
                if (hasFocus) {
                    reminderTitle.setCursorVisible(true);
                } else {
                    reminderTitle.setCursorVisible(false);
                    InputMethodManager softKeyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (softKeyboard != null) {
                        softKeyboard.hideSoftInputFromWindow(reminderTitle.getWindowToken(), 0);
                    }
                }
            }
        });

        reminderDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                EditText reminderDescription = v.findViewById(R.id.reminderDescription);
                if (hasFocus) {
                    reminderDescription.setCursorVisible(true);
                } else {
                    reminderDescription.setCursorVisible(false);
                    InputMethodManager softKeyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (softKeyboard != null) {
                        softKeyboard.hideSoftInputFromWindow(reminderDescription.getWindowToken(), 0);
                    }
                }
            }
        });

        inputField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    inputField.setCursorVisible(true);
                } else {
                    inputField.setCursorVisible(false);
                    InputMethodManager softKeyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (softKeyboard != null) {
                        softKeyboard.hideSoftInputFromWindow(inputField.getWindowToken(), 0);
                    }
                }
            }
        });

        Button prev = findViewById(R.id.buttonPrev);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Button create = findViewById(R.id.buttonCreate);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText reminderTitle = findViewById(R.id.reminderTitle);
                EditText reminderDescription = findViewById(R.id.reminderDescription);
                Switch alarmSwitch = findViewById(R.id.toggleAlarm);
                String reminderTitleText = reminderTitle.getText().toString();
                String reminderDescriptionText = reminderDescription.getText().toString();

                if (reminderTitleText.matches("")) {
                    ShowToastInIntentService(getString(R.string.error_reminder_no_title_set));
                    return;
                } else if (reminderDescriptionText.matches("")) {
                    ShowToastInIntentService(getString(R.string.error_reminder_no_description_set));
                    return;
                } else {
//                    Boolean result = writeToDatabase(reminderTitleText, reminderDescriptionText);
                    Boolean result = saveCurrentRemindersData();
                    if (result) {
                        setAlarm(alarmSwitch, alarmSwitch.isChecked(), false);
                        ShowToastInIntentService(getString(R.string.reminder_create_text_success));
                        finish();
                    } else {
                        ShowToastInIntentService(getString(R.string.reminder_create_text_failure));
                    }
                }
            }
        });
    }
}
